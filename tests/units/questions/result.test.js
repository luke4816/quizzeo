import { expect } from "chai";
import { getResultFreeAnswer, getResultFromPropositions } from "../../../src/libs/questions/result.js";

describe("Result of question", () => {
    describe("Result of free answer", () => {
        it("Same answers", () => {
            const result = getResultFreeAnswer("france", "france");
            expect(result).equal(true);
        });

        it("User answer with one more letter", () => {
            const result = getResultFreeAnswer("frances", "france");
            expect(result).equal(true);
        });

        it("Wrong letter", () => {
            const result = getResultFreeAnswer("frence", "france");
            expect(result).equal(false);
        });

        it("User answer with one missing letter", () => {
            const result = getResultFreeAnswer("franc", "france");
            expect(result).equal(false);
        });

        it("Different answers", () => {
            const result = getResultFreeAnswer("france", "spain");
            expect(result).equal(false);
        });
    });

    describe("Result from propostions", () => {
        it("Same answers", () => {
            const result = getResultFromPropositions("france", "france");
            expect(result).equal(true);
        });

        it("Wrong answer", () => {
            const result = getResultFromPropositions("france", "spain");
            expect(result).equal(false);
        });
    });
});
