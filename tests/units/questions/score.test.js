import { expect } from "chai";
import {
    calculateScore,
    calculateScoreFourPropositions,
    calculateScoreFreeAnswer,
    calculateScoreTwoPropositions,
} from "../../../src/libs/questions/score.js";

describe("Calculate Score Question", () => {
    describe("Calculate score free answer", () => {
        it("Calculate score good answer", () => {
            const score = calculateScoreFreeAnswer("france", "france");
            expect(score).equal(5);
        });

        it("Calculate score wrong answer", () => {
            const score = calculateScoreFreeAnswer("france", "spain");
            expect(score).equal(0);
        });
    });

    describe("Calculate score two propositions", () => {
        it("Calculate score good answer", () => {
            const score = calculateScoreTwoPropositions("france", "france");
            expect(score).equal(1);
        });

        it("Calculate score wrong answer", () => {
            const score = calculateScoreTwoPropositions("france", "spain");
            expect(score).equal(0);
        });
    });

    describe("Calculate score four propositions", () => {
        it("Calculate score good answer", () => {
            const score = calculateScoreFourPropositions("france", "france");
            expect(score).equal(3);
        });

        it("Calculate score wrong answer", () => {
            const score = calculateScoreFourPropositions("france", "spain");
            expect(score).equal(0);
        });
    });

    describe("Calculate score", () => {
        it("Calculate score free answer good answer", () => {
            const score = calculateScore("france", "france", "free");
            expect(score).equal(5);
        });
        it("Calculate score free answer wrong answer", () => {
            const score = calculateScore("france", "spain", "free");
            expect(score).equal(0);
        });
        it("Calculate score two propositions good answer", () => {
            const score = calculateScore("france", "france", "two_propositions");
            expect(score).equal(1);
        });
        it("Calculate score two propositions wrong answer", () => {
            const score = calculateScore("france", "spain", "two_propositions");
            expect(score).equal(0);
        });
        it("Calculate score four propositions good answer", () => {
            const score = calculateScore("france", "france", "four_propositions");
            expect(score).equal(3);
        });
        it("Calculate score four propositions wrong answer", () => {
            const score = calculateScore("france", "spain", "four_propositions");
            expect(score).equal(0);
        });
    });
});
