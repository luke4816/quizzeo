# Quizzeo

## Repository Teacher
https://gitlab.com/rems2rems/quizzeomaalsi22

## Summary
This is a work in progress. Currently you can't play a quizz and nothing is stored in a database. I haven't find how to play different types of questions.

## Technologies
This application uses express.js with ejs as a view engine. It uses a MongoDB database. It uses Gitlab's feature flags with unleash-client to activate/desactivate features depending on the user and the environment

## Commands
### Start Application in development
`npm start`

### Start Application in production
`npm build`

### Start Docker Database
`npm run docker-create-db`
