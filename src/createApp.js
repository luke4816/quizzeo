import express from "express";
import { getQuestion } from "./libs/questions/index.js";
import { isEnabled } from "./featureFlag.js";

export const createApp = () => {
    const app = express();
    app.use(express.urlencoded({ extended: true }));
    app.use(express.static("src/public"));

    app.set("view engine", "ejs");
    app.set("views", "./src/views");

    app.get("/", async (req, res) => {
        res.render("index");
    });

    app.post("/quizz", async (req, res) => {
        // start the quizz
        res.redirect("/");
    });

    app.get("/question/:id", async (req, res) => {
        const { id } = req.params;
        const question = getQuestion(id);
        const type = question.type;
        const flag = question.flag;
        const country = question.country;

        switch (type) {
            case "country_from_flag":
                if (isEnabled("question_country_from_flag") && flag && country) {
                    res.render("questions/country_from_flag", { flag, country });
                } else {
                    res.redirect("/");
                }
                break;
            default:
                res.redirect("/");
        }
    });

    return app;
};
