export default class Country {
    /**
     * @private
     * @type {string}
     */
    _name;
    /**
     * @private
     * @type {string}
     */
    _flag;
    /**
     * @private
     * @type {string}
     */
    _capital;

    /**
     *
     * @param {string} name
     * @param {string} flag
     * @param {string} capital
     */
    constructor(name, flag, capital) {
        this._name = name;
        this._flag = flag;
        this._capital = capital;
    }

    /**
     * @public
     */
    get name() {
        return this._name;
    }

    /**
     * @public
     */
    get flag() {
        return this._flag;
    }

    /**
     * @public
     */
    get capital() {
        return this._capital;
    }
}
