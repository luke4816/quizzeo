import Country from "./Country";

/**
 * @returns {Country[]}
 */
export const getCountries = async () => {
    const countries = [];

    const res = await fetch("https://restcountries.com/v3.1/all?fields=name,flags", { method: "GET" });

    const json = await res.json();

    json.forEach((country) => {
        const name = country["name"]["common"];
        const flag = country["flags"]["png"];
        const capital = country["capital"][0];
        countries.push(new Country(name, flag, capital));
    });

    return countries;
};
