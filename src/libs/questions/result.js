import stringSimilarity from "string-similarity";

/**
 *
 * @param {string} userAnswer
 * @param {string} expectedAnswer
 * @returns {boolean}
 */
export const getResultFreeAnswer = (userAnswer, expectedAnswer) => {
    const similarity = stringSimilarity.compareTwoStrings(userAnswer, expectedAnswer);
    const THRESHOLD = 0.9;

    return similarity >= THRESHOLD;
};

/**
 *
 * @param {string} userAnswer
 * @param {string} expectedAnswer
 * @returns {boolean}
 */
export const getResultFromPropositions = (userAnswer, expectedAnswer) => {
    return userAnswer === expectedAnswer;
};
