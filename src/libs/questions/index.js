/**
 *
 * @param {string} id
 * @returns {Question}
 */
export const getQuestion = (id) => {
    /**
     * @typedef {"country_from_flag"|"flag_from_country"|"capital_from_country"|"country_from_capital"} TypeQuestion
     * @type {TypeQuestion}
     */
    const type = "country_from_flag";
    const flag = "https://flagcdn.com/w320/fr.png";
    const country = "France";

    return { type, flag, country };
};
