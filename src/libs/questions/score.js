import { getResultFreeAnswer, getResultFromPropositions } from "./result.js";

/**
 *
 * @param {string} userAnswer
 * @param {string} expectedAnswer
 * @param {"free"|"two_propositions"|"four_propositions"} typeAnswer
 * @returns {number}
 */
export const calculateScore = (userAnswer, expectedAnswer, typeAnswer) => {
    let score = 0;

    switch (typeAnswer) {
        case "free":
            score = calculateScoreFreeAnswer(userAnswer, expectedAnswer);
            break;
        case "two_propositions":
            score = calculateScoreTwoPropositions(userAnswer, expectedAnswer);
            break;
        case "four_propositions":
            score = calculateScoreFourPropositions(userAnswer, expectedAnswer);
            break;
        default:
            break;
    }

    return score;
};

/**
 *
 * @param {string} userAnswer
 * @param {string} expectedAnswer
 * @returns {number}
 */
export const calculateScoreFreeAnswer = (userAnswer, expectedAnswer) => {
    let score = 0;
    const success = getResultFreeAnswer(userAnswer, expectedAnswer);
    if (success) {
        score = 5;
    }
    return score;
};

/**
 *
 * @param {string} userAnswer
 * @param {string} expectedAnswer
 * @returns {number}
 */
export const calculateScoreTwoPropositions = (userAnswer, expectedAnswer) => {
    let score = 0;
    const success = getResultFromPropositions(userAnswer, expectedAnswer);
    if (success) {
        score = 1;
    }
    return score;
};

/**
 *
 * @param {string} userAnswer
 * @param {string} expectedAnswer
 * @returns {number}
 */
export const calculateScoreFourPropositions = (userAnswer, expectedAnswer) => {
    let score = 0;
    const success = getResultFromPropositions(userAnswer, expectedAnswer);
    if (success) {
        score = 3;
    }
    return score;
};
