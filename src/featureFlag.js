import { startUnleash } from "unleash-client";

const ffUrl = "https://gitlab.com/api/v4/feature_flags/unleash/52941750";
let userId = process.env.UNLEASH_USER_ID;

export const initFeatureFlagManager = async () => {
    const ffAppName = process.env.UNLEASH_APP_NAME;
    const ffServerKey = process.env.UNLEASH_SERVER_KEY;
    userId = process.env.UNLEASH_USER_ID;

    const manager = await startUnleash({ url: ffUrl, appName: ffAppName, instanceId: ffServerKey });

    manager.on("ready", () => console.log("feature flag manager ready"));

    manager.on("error", console.error);

    return manager;
};

export const isEnabled = async (feature) => {
    const manager = await initFeatureFlagManager();

    return manager.isEnabled(feature, { userId: userId });
};
