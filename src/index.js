import dotenv from "dotenv";
dotenv.config();

import { createApp } from "./createApp.js";
import { connectDB } from "./db.js";

const app = createApp();

await connectDB();

const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Application started on port ${PORT}`);
});
